const form = document.querySelector(".password-form");
const passwordInput = form.querySelector(".password");
const confirmPasswordInput = form.querySelector(".password-confirm");
const error = form.querySelector(".error");

form.addEventListener("submit", function(e) {
  e.preventDefault();
  if (passwordInput.value !== confirmPasswordInput.value) {
    error.textContent = "Потрібно ввести однакові значення";
    error.style.color = "red";
    error.style.display = "block";
  } else {
    alert("You are welcome");
    error.style.display = "none";
  }
});

form.addEventListener("click", function(e) {
  if (e.target.classList.contains("fa-eye-slash") || e.target.classList.contains("fa-eye")) {
    const icon = e.target;
    icon.classList.toggle("fa-eye-slash");
    icon.classList.toggle("fa-eye");
    const inputs = icon.previousElementSibling;
    inputs.type = icon.classList.contains("fa-eye") ? "text" : "password";
  }
});
  
  

/*
1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

Экранирование служит для возврата специальному символу его обычного строкового значения. Например,
если кавычки встречаются в строке, они воспринимаются как спецсимвол окончания этой строки. Если мы их экранируем,
им возвращается обычное строчное значение в составе этой строки, они перестают быть спецсимволом.

2.Які засоби оголошення функцій ви знаєте?
Function declaration при помощи ключевого слова function
Function expression также при помощи слова function, но она не записывается в переменную

3.Що таке hoisting, як він працює для змінних та функцій?
hoisting — это механизм , в котором переменные и объявления функций, передвигаются вверх своей области видимости перед тем, как код будет выполнен.*/

function CreateNewUser() {
  this.firstName = prompt("Type your first Name");
  this.lastName = prompt("Type your last Name");
  let getBirthday = new Date(prompt("type date of your birthday","mm,dd,year"));
  this.birthday = getBirthday.toLocaleDateString();
  
  this.getLogin = function () {
    return `${this.firstName.charAt(0)}${this.lastName.toLowerCase()} `;
  };
  this.getYear = function(){
    this.year = getBirthday.getFullYear()
    return this.year
  }
  this.getAge = function(){
    const dateNow = new Date();
    const diff = new Date(dateNow.valueOf() - getBirthday.valueOf())
    let age ;
    return age = Math.abs(diff.getFullYear() - 1970)
  }
  this.getPassword = function(){
    return `${this.firstName.charAt(0).toUpperCase()}${this.lastName.toLowerCase()}${this.year}`;
  }
}
const newUser = new CreateNewUser();
console.log(newUser)
console.log(newUser.getLogin())
console.log(newUser.getYear())
console.log(newUser.getAge())
console.log(newUser.getPassword())




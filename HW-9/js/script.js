/* 
1.Опишіть, як можна створити новий HTML тег на сторінці.

сonst div = document.createElement("div")//создает элемент в скобках название тега 
body.append(div); // после создания можно добавить созданный элемент на страницу (иначе уже созданный элемент не будет отображаться на странице)

2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

Первый параметр "куда вставить" варианты : 
beforebegin- вставить html перед elem,
afterbegin- вставить html в начало elem,
beforeend- вставить html в конец elem,
afterend- вставить html  после elem. 

Второй параметр вставить HTML именно "как html", со всеми тегами и прочим;
Например :
document.body.insertAdjacentHTML("afterbegin", `<div class="greeting"> <strong>Всем привет!</strong> HELLO HELLO</div>`); 

3.Як можна видалити елемент зі сторінки? 
element.remove() удалить элемент
*/

function createListContent(array, parent) {
  const ul = document.createElement("ul");
  array.forEach((element) => {
    if (typeof element === "object") {
      createListContent(element, ul)
    }
    else {
      const li = document.createElement('li');
      li.textContent = element;
      ul.append(li);
    }
  });
  parent.append(ul);
}

const citys = ["Lviv", "Kiev", ["Borispol", "Irpin"], "Zaporizhia", "Odessa", "Kharkiv"];
const body = document.body;
createListContent(citys, body)




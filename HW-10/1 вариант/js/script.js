

function openContent(evt, contentId) {
  
  const tabcontent = document.querySelectorAll(".tabcontent");
  for (let i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  const  tablinks = document.querySelectorAll(".tabs-title");
  for (let i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(contentId).style.display = "block";
  evt.currentTarget.className += " active";
}


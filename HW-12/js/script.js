/* Чому для роботи з input не рекомендується використовувати клавіатуру?

Использование клавиатуры для работы с input может привести к проблемам с доступностью и безопасностью. 
Люди с ограниченной мобильностью могут иметь трудности с вводом данных с клавиатуры, а использование клавиатуры может позволить пользователям
вводить в систему вредоностный код.  */


const btns = Array.from(document.querySelectorAll(".btn"));
const body = document.body;
let previousSelectedBtn;
 
body.addEventListener("keydown", e => {
  const selectedBtn = btns.find(btn => btn.getAttribute('data-key') === e.key);
  if (selectedBtn) {
    if (previousSelectedBtn) {
      previousSelectedBtn.style.backgroundColor = "black";
    }
    selectedBtn.style.backgroundColor = "blue";
    previousSelectedBtn = selectedBtn;
  }
});



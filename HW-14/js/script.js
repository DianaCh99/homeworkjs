window.onload = function () {
const themeBtn = document.querySelector(".change-theme-btn");
themeBtn.addEventListener('click', () => {
  document.body.toggleAttribute("theme");
  if (document.body.hasAttribute("theme")) {
    localStorage.setItem("theme", "new-theme")
  } else {
    localStorage.removeItem("theme")
  }
});
console.log(localStorage);

  if (localStorage.getItem("theme")) {
    document.body.setAttribute("theme", "new-theme")
  }
}
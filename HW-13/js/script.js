/* 
1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
В общем, setTimeout используется для запуска одной задачи после определенного времени, в то время как setInterval 
используется для запуска задачи непрерывно через заданный интервал времени.
2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Если передать 0  в функцию setTimeout, то задача, которую нужно  выполнить, будет помещена в очередь событий и выполнена сразу по наступлению ее очереди. 
3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен? 
если  не остановить ранее созданный цикл запуска, то цикл продолжит запускаться вечно. 
Это может привести к перегрузке процессора и поэтому важно вызывать clearInterval()
*/


let counter = 0;
const pictures = document.querySelectorAll(".image-to-show");
const btns = document.querySelectorAll("button");
const stopBtn =document.querySelector(".stop-slideShow");
const startBtn =document.querySelector(".start-slideShow")

function displayBtns() {
  btns.forEach( btn => { 
    btn.style.display ="inline";
  })

}
setTimeout(displayBtns, 4000);

for (let i = 1; i < pictures.length; i++) {
    pictures[i].style.display = "none";
}
function displayPictures() {
    for (let i = 0; i < pictures.length; i++) {
        pictures[i].style.display = "none";
    }
    counter = (counter + 1) % pictures.length;
    pictures[counter].style.display = "block";
}

let timer =  setInterval(displayPictures, 3000);


startBtn.addEventListener("click",e=>{
  timer =  setInterval(displayPictures, 3000) });
  stopBtn.addEventListener("click", e=>{
  clearInterval(timer)});
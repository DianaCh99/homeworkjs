/* 
1.Опишіть своїми словами що таке Document Object Model (DOM)
программный интерфейс, позволяющий скриптам получить доступ к содержимому Html и изменять содержимое, структуру и оформление таких документов.

2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
 innerText устанавливает или возвращает текстовое содержимое в виде простого текста указанного узла и всех его потомков, 
 innerHTML получает и устанавливает обычный текст или содержимое HTML в элементах. innerHTML позволяет работать с форматированным 
 текстом HTML и не выполняет автоматическое кодирование и декодирование текста.

 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий? 
 Лучший способ : 
 querySelector() или querySelectorAll() 
- он удобнее так как в скобках можно использовать любой CSS-селектор.
 также есть способы :
  getElementById()
  getElementsByClassName()
  getElementsByName()
  getElementsByTagName()
*/


const p = document.querySelectorAll("p");
for (let i of document.querySelectorAll("p")) {
  i.style.backgroundColor = "#ff0000";
}

const list = document.querySelector("#optionsList");
console.log(list);

const parentOfList = list.parentElement;
console.log(parentOfList);

let childrens;
if (list.hasChildNodes()) {
  childrens = list.childNodes
}
console.log(childrens)

const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerText ="This is a paragraph";


const elementsOfHeader = Array.from(document.querySelector(".main-header").children)
console.log(elementsOfHeader)
elementsOfHeader.forEach(elem => elem.setAttribute("class", "nav-item"))

const sectionTitle = Array.from(document.querySelectorAll(".section-title"));
console.log(sectionTitle)
sectionTitle.forEach(elem => elem.removeAttribute("class"))